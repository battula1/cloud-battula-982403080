"""
A simple flask app.
"""
import flask
from flask.views import MethodView
from index import Index
from weather import Weather
from spotify import Spotify
from error import Error

# Create a Flask app instance
app = flask.Flask(__name__)

# Index route
# This route renders the main page containing the links to other routes.
app.add_url_rule('/',
                 view_func=Index.as_view('index'),
                 methods=["GET"])

# weather route
# This route renders the weather page which asks city from user to get the weather.
app.add_url_rule('/weather',
                 view_func=Weather.as_view('weather'),
                 methods=["GET", "POST"])

# spotify route
# This route renders the spotify page which gets the list of top 10 songs based on the weather.
app.add_url_rule('/spotify',
                 view_func=Spotify.as_view('spotify'),
                 methods=["GET", "POST"])

# error route
# This route renders the error page when wrong city is entered.
app.add_url_rule('/error',
                 view_func=Error.as_view('error'),
                 methods=["GET"])

if __name__ == '__main__':
    # Start the Flask development server
    app.run(host='0.0.0.0', port=8080, debug=True)
