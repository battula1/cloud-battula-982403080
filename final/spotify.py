from flask import render_template, request, redirect, url_for
from flask.views import MethodView
import spotipy
from spotipy.oauth2 import SpotifyClientCredentials
import os

# Set up Spotify credentials
SPOTIFY_CLIENT_ID = os.environ.get('SPOTIFY_CLIENT_ID')

SPOTIFY_CLIENT_SECRET = os.environ.get('SPOTIFY_CLIENT_SECRET')

# Initialize Spotipy client
auth_manager = SpotifyClientCredentials(client_id=SPOTIFY_CLIENT_ID, client_secret=SPOTIFY_CLIENT_SECRET)
sp = spotipy.Spotify(auth_manager=auth_manager)

class Spotify(MethodView):
    """
    Index class renders spotify.html

    Methods:
    - get(): Renders the spotify.html.
    """
    def get(self):
        mood = request.args.get('mood')
        if mood:
            # Call Spotify API to search for tracks based on mood
            results = sp.search(q=f'genre:"{mood}"', type='track', limit=10)
            top_songs = []
            for track in results['tracks']['items']:
                top_songs.append({
                    'name': track['name'],
                    'artists': ', '.join([artist['name'] for artist in track['artists']])
                })
        else:
            top_songs = []

        return render_template('spotify.html', mood=mood, top_songs=top_songs)