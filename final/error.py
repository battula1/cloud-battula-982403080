from flask import render_template
from flask.views import MethodView
import requests

class Error(MethodView):
    """
    Error class renders error.html

    Methods:
    - get(): Renders the error.html.
    """
    def get(self):
        return render_template('error.html')