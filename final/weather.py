from flask import render_template, request, redirect, url_for
from flask.views import MethodView
import requests
import os


class Weather(MethodView):
    """
    Weather class renders weather.html

    Methods:
    - get(): Renders the weather.html.
    """
    def get(self):
        return render_template('weather.html')
    
    def determine_mood(self, weather_data):
        # Extract relevant weather information
        weather_main = weather_data['weather'][0]['main']
        temperature = weather_data['main']['temp']
        
        # Determine mood or genre based on weather conditions
        if 'Clouds' in weather_main:
            if temperature > 20:
                return 'happy'  # 'happy' songs for cloudy and warm weather
            else:
                return 'chill'  # 'chill' songs for cloudy and cool weather
        elif 'Rain' in weather_main:
            return 'relax'  # 'relaxing' songs for rainy weather
        elif 'Clear' in weather_main:
            if temperature > 25:
                return 'party'  # 'party' songs for clear and hot weather
            else:
                return 'romantic'  # 'romantic' songs for clear and mild weather
        elif 'Thunderstorm' in weather_main:
            return 'rock'  # 'rock' songs for thunderstorm weather
        elif 'Snow' in weather_main:
            return 'mellow'  # 'mellow' songs for snowy weather
        elif 'Fog' in weather_main or 'Mist' in weather_main:
            return 'calm'  # 'calm' songs for foggy or misty weather
        else:
            return 'background'  # Default mood or genre for other weather conditions


    def post(self):
        """
        Accepts POST requests, and processes the form;
        Redirect to spotify when completed.
        """
        city = request.form['city']
        api_key = os.environ.get('WEATHER_API_KEY')
        print(city)
        url = f'http://api.openweathermap.org/data/2.5/weather?q={city}&appid={api_key}&units=metric'
        print(url)
        response = requests.get(url)
        print(response.status_code)
        if response.status_code == 404:
            # City not found, redirect to error page
            return redirect(url_for('error'))

        data = response.json()

        print(data)
        mood = self.determine_mood(data)
        print(mood)
        return redirect(url_for('spotify', mood=mood))