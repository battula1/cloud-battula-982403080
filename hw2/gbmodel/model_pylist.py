"""
Python list model
"""
from datetime import date
from .Model import Model

class model(Model):
    def __init__(self):
        self.quotes = []

    def select(self):
        """
        Returns quotes list of lists
        Each list in quotes contains: quote, person, date, source_type, source, rating
        :return: List of lists
        """
        return self.quotes

    def insert(self, quote, person, date, source_type, source, rating):
        """
        Appends a new list of values representing new message into guestentries
        :param quote: String
        :param person: String
        :param date: date
        :param source_type: String
        :param source: String
        :param rating: String
        :return: True
        """
        params = [quote, person, date, source_type, source, rating]
        self.quotes.append(params)
        return True
        
    def delete(self, quote):
        """
        Deletes entry from the quotes list based on quote
        :param quote: String
        :return: True if the entry was deleted, False otherwise
        """
        for entry in self.quotes:
            if entry[0] == quote:
                self.quotes.remove(entry)
                return True
        return False

    def update(self, old_quote, new_quote, person, date, source_type, source, rating):
        """
        Updates entry in the quotes list based on old quote
        :param old_quote: String
        :param new_quote: String
        :param person: String
        :param date: date
        :param source_type: String
        :param source: String
        :param rating: String
        :return: True if the entry was updated, False otherwise
        """
        for i, entry in enumerate(self.quotes):
            if entry[0] == old_quote:
                self.quotes[i] = [new_quote, person, date, source_type, source, rating]
                return True
        return False