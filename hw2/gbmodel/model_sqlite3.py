"""
A simple quote flask app.
Data is stored in a SQLite database that looks something like the following:

+----------------+------------------+------------+----------------+----------+----------
| quote          | person           | date       | source_type    | source   |  rating |
+================+==================+============+================+==========+==========
| The road to    |                  |            |                |          |         |
| hell is paved  | Stephen King     | 2012-05-28 | speech         | sppech   |   3     |
| with adverbs.  |                  |            |                |          |         |
+----------------+------------------+------------+----------------+----------+---------+

This can be created with the following SQL (see bottom of this file):

    create table guestbook (quote text, person text, date date, source_type text, source text, rating text);

"""
from datetime import date
from .Model import Model
import sqlite3
DB_FILE = 'entries.db'    # file for our Database

class model(Model):
    def __init__(self):
        # Make sure our database exists
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        try:
            cursor.execute("select count(rowid) from quotes")
        except sqlite3.OperationalError:
            cursor.execute("CREATE TABLE quotes (quote TEXT, person TEXT, date DATE, source_type TEXT, source TEXT, rating TEXT)")
        cursor.close()

    def select(self):
        """
        Gets all rows from the database
        Each row contains: name, person, date, source_type, source, rating
        :return: List of lists containing all rows of database
        """
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM quotes")
        return cursor.fetchall()

    def insert(self, quote, person, date, source_type, source, rating):
        """
        Inserts entry into database
        :param quote: String
        :param person: String
        :param date: date
        :param source_type: String
        :param source: String
        :param rating: String
        :return: True
        :raises: Database errors on connection and insertion
        """
        params = {'quote':quote, 'person':person, 'date':date, 'source_type':source_type, 'source': source, 'rating':rating}
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("insert into quotes (quote, person, date, source_type, source, rating) VALUES (:quote, :person, :date, :source_type, :source, :rating)", params)

        connection.commit()
        cursor.close()
        return True

    def delete(self, quote):
        """
        Deletes entry from the database based on quote
        :param quote: String
        :return: True
        :raises: Database errors on connection and deletion
        """
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        cursor.execute("DELETE FROM quotes WHERE quote = :quote", {'quote': quote})
        connection.commit()
        cursor.close()
        return True

    def update(self, old_quote, new_quote, person, date, source_type, source, rating):
        """
        Updates entry in the database based on old quote
        :param old_quote: String
        :param new_quote: String
        :param person: String
        :param date: date
        :param source_type: String
        :param source: String
        :param rating: String
        :return: True
        :raises: Database errors on connection and update
        """
        connection = sqlite3.connect(DB_FILE)
        cursor = connection.cursor()
        update_query = "UPDATE quotes SET quote = :new_quote, person = :person, date = :date, source_type = :source_type, source = :source, rating = :rating WHERE quote = :old_quote"
        cursor.execute(update_query, {'old_quote': old_quote, 'new_quote': new_quote, 'person': person, 'date': date, 'source_type': source_type, 'source': source, 'rating': rating})
        connection.commit()
        cursor.close()
        return True