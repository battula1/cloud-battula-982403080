class Model():
    def select(self):
        """
        Gets all entries from the database
        :return: Tuple containing all rows of database
        """
        pass

    def insert(self, name, email, message):
        """
        Inserts entry into database
        :param quote: String
        :param person: String
        :param date: date
        :param source_type: String
        :param source: String
        :param rating: String
        :return: none
        :raises: Database errors on connection and insertion
        """
        pass
    
    def delete(self, quote):
        """
        Deletes entry from the database based on quote
        :param quote: String
        :return: True
        :raises: Database errors on connection and deletion
        """
        pass 

    def update(self, old_quote, new_quote, person, date, source_type, source, rating):
        """
        Updates entry in the database based on old quote
        :param old_quote: String
        :param new_quote: String
        :param person: String
        :param date: date
        :param source_type: String
        :param source: String
        :param rating: String
        :return: True
        :raises: Database errors on connection and update
        """
        pass