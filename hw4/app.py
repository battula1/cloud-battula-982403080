"""
A simple flask app.
"""
import flask
from flask.views import MethodView
from index import Index
from entries import Entry
from quotes import Quote
from delete import Delete
from update import Update

# Create a Flask app instance
app = flask.Flask(__name__)

# Index route
# This route renders the main page containing the links to other routes.
app.add_url_rule('/',
                 view_func=Index.as_view('index'),
                 methods=["GET"])

# Entries route
# This route is for displaying all entries in the quote table.
app.add_url_rule('/entries',
                view_func=Entry.as_view('entries'),
                methods=["GET"])

# Quotes route
# This route handles inserting quote information to the quote table.
app.add_url_rule('/quotes',
                 view_func=Quote.as_view('quotes'),
                 methods=['GET', 'POST'])

# Delete route
# This route allows users to delete entries from the quote table.
app.add_url_rule('/delete',
                 view_func=Delete.as_view('delete'),
                 methods=['GET', 'POST'])

# Update route
# This route allows users to update existing entries in the quote table.
app.add_url_rule('/update',
                 view_func=Update.as_view('update'),
                 methods=['GET', 'POST'])

if __name__ == '__main__':
    # Start the Flask development server
    app.run(host='0.0.0.0', port=5000, debug=True)
