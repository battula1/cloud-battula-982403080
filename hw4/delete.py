from flask import redirect, request, url_for, render_template
from flask.views import MethodView
import gbmodel

class Delete(MethodView):
    """
    Delete class is to delete an entry in quotes table based on the quote

    Methods:
    - get(): Renders the delete.html.
    - post(): deletes a particular entry in the quotes table and redirects to index.html.
    """
    def get(self):
        return render_template('delete.html')

    def post(self):
        """
        Accepts POST requests, and processes the form;
        Redirect to index when completed.
        """
        model = gbmodel.get_model()
        model.delete(request.form['quote'])
        return redirect(url_for('index'))