from flask import redirect, request, url_for, render_template
from flask.views import MethodView
import gbmodel

class Quote(MethodView):
    """
    Quote class helps inserting the data into quotes table

    Methods:
    - get(): Renders the quotes.html.
    - post(): inserts the data into quotes table based on user input and redirects to landing.html.

    """
    def get(self):
        return render_template('quotes.html')

    def post(self):
        """
        Accepts POST requests, and processes the form;
        Redirect to index when completed.
        """
        model = gbmodel.get_model()
        model.insert(request.form['quote'], request.form['person'], request.form['date'], request.form['source_type'], request.form['source'],request.form['rating'])
        return redirect(url_for('index'))
