from .Model import Model
from datetime import datetime
from google.cloud import datastore

def from_datastore(entity):
    """Translates Datastore results into the format expected by the
    application.

    Datastore typically returns:
        [Entity{key: (kind, id), prop: val, ...}]

    This returns:
        [ quote, person, date, source_type, source, rating ]
    where quote, person, source_type, source, rating are Python strings
    and where date is a Python datetime
    """
    if not entity:
        return None
    if isinstance(entity, list):
        entity = entity.pop()
    return [entity['quote'],entity['person'],entity['date'],entity['source_type'], entity['source'], entity['rating']]

class model(Model):
    def __init__(self):
        self.client = datastore.Client('cloud-battula-982403080')

    # retrives all the rows from the data store
    def select(self):
        query = self.client.query(kind = 'hw4')
        entities = list(map(from_datastore,query.fetch()))
        return entities

    # inserts a row with quote information into the data store
    def insert(self,quote,person,date, source_type, source, rating):
        key = self.client.key('hw4')
        rev = datastore.Entity(key)
        rev.update( {
            'quote': quote,
            'person' : person,
            'date' : datetime.today(),
            'source_type' : source_type,
            'source' : source,
            'rating' : rating
            })
        self.client.put(rev)
        return True

    # deletes a row from data store based on the quote  
    def delete(self, quote):
        query = self.client.query(kind='hw4')
        query.add_filter('quote', '=', quote)
        result = query.fetch(limit=1)
        entities = list(result)

        if entities: # Quote is found
            self.client.delete(entities[0].key) # delete the quote
            return True
        else:
            return False  # Quote not found

    # updates quote's information in data store based on the quote 
    def update(self, old_quote, new_quote, person, date, source_type, source, rating):
        query = self.client.query(kind='hw4')
        query.add_filter('quote', '=', old_quote)
        result = query.fetch(limit=1)
        entities = list(result)
        
        if entities:
            entity = entities[0]
            entity['quote'] = new_quote
            entity['person'] = person
            entity['date'] = date
            entity['source_type'] = source_type
            entity['source'] = source
            entity['rating'] = rating
            self.client.put(entity)
            return True
        else:
            return False  # Old quote not found

