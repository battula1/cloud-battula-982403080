from flask import redirect, request, url_for, render_template
from flask.views import MethodView
import gbmodel

class Update(MethodView):
    """
    Update class helps updating the columns for quotes table

    Methods:
    - get(): Renders the update.html.
    - post(): updates the quotes table based on user input and redirects to landing.html.

    """
    def get(self):
        return render_template('update.html')

    def post(self):
        """
        Accepts POST requests, and processes the update form;
        Redirect to landing when completed.
        """
        model = gbmodel.get_model()
        model.update(request.form['old_quote'], request.form['new_quote'], request.form['person'], request.form['date'], request.form['source_type'], request.form['source'],request.form['rating'])
        return redirect(url_for('index'))