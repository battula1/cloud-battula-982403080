from flask import render_template
from flask.views import MethodView

class Index(MethodView):
    """
    Index class renders landing.html

    Methods:
    - get(): Renders the landing.html.
    """
    def get(self):
        return render_template('landing.html')
