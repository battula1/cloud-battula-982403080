from flask import render_template
from flask.views import MethodView
import gbmodel

class Entry(MethodView):
    """
    Entry class renders entries.html

    Methods:
    - get(): gets all the rows from quotes table and sends the data to entries.html.

    """
    def get(self):
        model = gbmodel.get_model()
        entries = [dict(quote=row[0], person=row[1], date=row[2], source_type=row[3], source=row[4], rating=row[5]) for row in model.select()]
        return render_template('entries.html',entries=entries)